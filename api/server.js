const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
app.use(cors());

// declare env to access the environment package
require('dotenv').config();
const PORT = process.env.PORT;
const DB =process.env.MONGODB;

// our mongoose connection
mongoose.connect(DB, {
	useNewUrlParser:true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', ()=>{
	console.log('Connected to our localhost database');
})

app.use(express.json());
app.use(express.urlencoded({extended:true}))

// import the routes
const userRoutes = require('./routes/users')
const transactionRoutes = require('./routes/transactions')
const categoryRoutes = require('./routes/category')

// assign the imported routes
app.use('/api/users', userRoutes)
app.use('/api/transactions', transactionRoutes)
app.use('/api/category',categoryRoutes)


// initialize the server
app.listen(PORT, ()=>{
	console.log(`You are now connected to port ${PORT}`)
})