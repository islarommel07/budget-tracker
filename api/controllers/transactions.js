const Transaction = require('../models/transactions');
const User = require('../models/users')
const Category = require('../models/category')

// create transaction

module.exports.add = (params) => {
	const newTransaction = new Transaction({
		transaction: [{
			categoryName: params.categoryName,
			description: params.description,
			amount: params.amount
		}]
	})
	return User.findById(params.userId).then((user,err) => {
		return newTransaction.save().then((addedTransaction, err) => {
			user.transaction.push({
				transactionId: addedTransaction._id,
				categoryName: params.categoryName,
				description: params.description,
				amount: params.amount
			})
			console.log(user.transaction)
			console.log(addedTransaction)
			return user.save().then((updatedUser, err) => {
				addedTransaction.transactors.push({
				userId: params.userId,
				transactionDate: addedTransaction.createdAt
			})
				return addedTransaction.save().then((addedTransaction,err) => {
					// console.log(user)
					return (err) ? false : addedTransaction
				})
			})
		})
	})

}


// retrieve transaction
module.exports.getAll = ()=>{
	return Transaction.find().then((transactions,err)=>{
		return (err) ? false : transactions
	})
}

module.exports.getOne = (transactionID) =>{
	return Transaction.findById(transactionID).then((transaction,err)=>{
		return(err)? false : transaction
	})
}

// update transaction
module.exports.update = (params)=>{
	return Transaction.findById(params.transactionID).then((updates,err)=>{
		if(err){
			return false
		} else {
			// console.log(updates.category[0].name)

			updates.transaction[0].transactionName = params.transaction[0].transactionName
			updates.transaction[0].description = params.transaction[0].description
			updates.transaction[0].amount = params.transaction[0].amount

			return User.findById(params.userId).then((user, err) => {
				user.transaction.splice(params.transactionId, 1)
				updates.transactors.push({
					userId: params.userId,
					transactionDate: params.createdAt
				})
				return updates.save().then((updatedTransaction, err) => {
					user.transaction.push({
						transactinId: updatedTransaction._id,
						description: updatedTransaction.description,
						amount: updatedTransaction.amount
					})
					return user.save().then((updatedUser, err) => {
						// console.log(updatedTransaction)
						return (err) ? false : updatedUser
					})
				})
			})
		}
	})
}

// delete transaction
module.exports.archive = (params) =>{
	return Transaction.findById(params.transactionID).then((transaction, err)=>{
		if (transaction.isActive===true){
			transaction.isActive = false
		} else {
			transaction.isActive = true
		}
		return transaction.save().then((transaction,err)=>{
			return User.findById(params.userId).then((user,err) => {
				if(user.transaction[0].isActive === true){
					user.transaction[0].isActive = false
				} else {
					user.transaction[0].isActive =true
				}
				return user.save().then((archivedTransaction,err) => {
					return (err) ? false : archivedTransaction
				})
			})
		})
	})
}