const Category = require('../models/category');
const User = require('../models/users');

// create category
module.exports.create = (params)=>{
	const newCategory = new Category({
		category: [{
			name: params.name,
			type: params.type
		}]
		
	})
	return User.findById(params.userId).then((user,err) => {
		console.log(user)
		return newCategory.save().then((category,err) => {
			user.category.push({
				categoryId: category._id,
				name: category.name,
				type: category.type
			})
			return user.save().then((newCategory,err) => {
				console.log(newCategory)
				return (err) ? false : newCategory
			})
		})
	})
}


// retrieve category
module.exports.getOne = (categoryID) =>{
	return Category.findById(categoryID).then((result, err)=>{
		return (err) ? false : result
	})
}
module.exports.getAll = () => {
	return Category.find().then((categories,err) => {
		return (err) ? false : categories
	})
}
// update category
module.exports.update = (params)=>{
	return Category.findById(params.categoryID).then((updates,err)=>{
		if(err){
			return false
		} else {
			updates.category[0].name = params.category[0].name
			updates.category[0].type = params.category[0].type
			
		}
		return User.findById(params.userId).then((user,err) => {
			user.category.splice(params.categoryId, 1)
			return updates.save().then((updatedCategory,err) => {
				user.category.push({
					categoryId: updatedCategory._id,
					name: updatedCategory.name,
					type: updatedCategory.type
				})
				return user.save().then((updatedUser,err) => {
					return (err) ? false : updatedUser
				})
			})
		})
	})
}

// delete category
// module.exports.archive = (params) => {
// 	return Category.findById(params.categoryID).then((category,err) => {
// 		if(category.isActive ===true){
// 			category.isActive = false
// 		} else {
// 			category.isActive = true
// 		}
// 		return category.save().then((archivedCategory,err) => {
// 			return User.findById(params.userId).then((user, err) => {
// 				if(user.category[0].isActive===true){
// 					user.category[0].isActive = false
// 				} else {
// 					user.category[0].isActive = true
// 				}
// 				return user.save().then((user, err) => {
// 					console.log(archivedCategory)
// 					return (err) ? false : user
// 				})
// 			})
// 		})
// 	})
// }

module.exports.archive = (params) => {
	return Category.findById(params.categoryID).then((category,err) => {
		if(category.isActive===true){
			category.isActive = false
		} else {
			category.isActive = true
		}
		return category.save().then((archivedCategory,err) => {
			return User.findById(params.userId).then((user,err) => {
				if(user.category[0].isActive === true) {
					user.category[0].isActive = false
				} else {
					user.category[0].isActive = true
				}
				return user.save().then((updatedUser,err) => {
					console.log(archivedCategory)
					return (err) ? false : updatedUser
				})
			})
		})
	})
}