const User = require('../models/users');
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {OAuth2Client} = require('google-auth-library')
const clientId = '438451252412-n9cqnsgcrkqng28dfuaas3l72qh752fo.apps.googleusercontent.com'


module.exports.register = (params) => {
	return User.findOne({email: params.email}).then(user=>{
		if(user !== null){
			if(user.loginType === 'google'){
				return{error:'use-google-login'}
			} else {
				// console.log(user)
				return{error: 'account-already-registered'}
			}
		} else {
			const newUser = new User({
				firstName : params.firstName,
				lastName : params.lastName,
				email : params.email,
				password :bcrypt.hashSync(params.password, 10),
				loginType: 'manual'
			})
			return newUser.save().then((user,err)=>{
				return (err)? err : user
	})
		}
	})

}

module.exports.login = (user) =>{
	// console.log(user)
	return User.findOne({email: user.email}).then(returnedUser =>{
		if(returnedUser === null){
			return false
		}
		const isPasswordMatched = bcrypt.compareSync(user.password, returnedUser.password)
		if(isPasswordMatched){
			return{
				accessToken: auth.createAccessToken(returnedUser.toObject())
			}
		} else {
			return false
		}
	})
}

// retrieve user
module.exports.getUserDetails = (params) =>{
	return User.findById(params.userID).then((returnedUser,err)=>{
		if (err){
			return false
		} else {
			returnedUser.password = undefined
			console.log(returnedUser)
			return returnedUser
		}
	})
}
		

// module.exports.verifyGoogleTokenId = async (tokenId) =>{
// 	const client = new OAuth2Client(clientId)
// 	const data = await client.verifyIdToken({
// 		idToken: tokenId,
// 		audience: clientId
// 	})
// 	if (data.payload.email_vefified===true){
// 			const newUser = newUser({
// 				firstName:data.payload.given_name,
// 				lastName: data.payload.family_name,
// 				email: data.payload.email,
// 				loginType: 'manual'
// 			})
// 		return User.findOne({email:data.payload.email}).then((user,err)=>{
// 			return (err) ?
// 				false
// 				:(user !==null) ?
// 					(user.loginType === 'google') ?
// 					{accessToken:auth.createAccessToken(user,toObject()),
// 						_id:user._id,
// 						email:user.email
// 					}
// 					: {error: 'login-type-error'}
// 					: "value", newUser.save().then((user,err)=>{
// 						return {accessToken: auth.createAccessToken(user.toObject()),
// 							_id:user._id,
// 							email:user.email
// 						}
// 					})
// 		})
// 		} else {
// 			return {error: 'google-auth-error'}
// 		}

// }

module.exports.verifyGoogleTokenId = async (tokenId) => {
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})
	if(data.payload.email_vefified === true) {
		return User.findOne({email: data.payload.email}).then((user,err)=>{
			if(user !== null) {
				if(user.loginType === 'google') {
					return{accessToken: auth.createAccessToken(user.toObject())}
				} else {
					return {error: 'login-type-error'}
				}
			} else {
				const newUser = new User({
				firstName : data.payload.given_name,
				lastName : data.payload.family_name,
				email : data.payload.email,
				loginType: 'google'
				})
			return newUser.save().then((user, err) => {
				return {accessToken: auth.createAccessToken(user.toObject())}
			})
			}
		})
	} else {
		return {error: 'google-auth-error'}
	}
}

// update user
module.exports.update = (params) =>{
	return User.findById(params.userID).then((user,err)=>{
		if(err){
			return false
		} else {
			user.firstName = params.firstName
			user.lastName = params.lastName
			user.email = params.email
			user.password = params.password
			user.currentBalance = params.currentBalance

			return user.save().then((user,err)=>{
				return (err) ? false : user
			})
		}
	})
}


// delete user
module.exports.archive = (userID) =>{
	return User.findById(userID).then((user,err)=>{
		if(err){
			return false
		} else
		{
			if(user.isActive){
				user.isActive = false
			} else {
				user.isActive = true
			}
			return user.save().then((archivedUser, err) =>{
				return (err) ? false : archivedUser
			})
		}
	})
}


