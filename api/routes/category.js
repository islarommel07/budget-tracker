const express = require('express')
const router = express.Router();
const CategoryController = require('../controllers/category')
const auth = require('../auth')

router.post('/', auth.verify, (req,res)=>{
	const args = {
		userId: auth.decode(req.headers.authorization).id,
		name: req.body.name,
		type: req.body.type
	}
	return CategoryController.create(args).then(result => res.send(result))
})

router.get('/:categoryID', (req,res)=>{
	return CategoryController.getOne(req.params.categoryID).then(result=> res.send(result))
})

router.get('/', (req, res) => {
	return CategoryController.getAll(req.params).then(result => res.send(result))
})

router.put('/:categoryID', auth.verify, (req,res) => {
	const args = {
		userId: auth.decode(req.headers.authorization).id,
		categoryID: req.params.categoryID,
		category:[{
			name: req.body.name,
			type: req.body.type
		}]
	}
	return CategoryController.update(args).then(result => res.send(result))
})

router.delete('/:categoryID', auth.verify, (req,res) => {
	const args = {
		userId: auth.decode(req.headers.authorization).id,
		categoryID: req.params.categoryID
	}
	CategoryController.archive(args).then(result => res.send(result))
})

module.exports = router