const express = require('express');
const router = express.Router();
const UserController = require('../controllers/users')
const auth = require('../auth')

// create user account	
router.post('/', (req,res)=>{
	UserController.register(req.body).then(result => res.send(result))
})

router.post('/login', (req,res)=>{
	UserController.login(req.body).then(result => res.send(result))
})

router.get('/details', auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	UserController.getUserDetails({userID: user.id}).then(user=> res.send(user))
})

router.post('/verify-google-id-token', async (req,res)=>{
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

router.put('/:userID', (req,res)=>{
	const args = {
		userID: req.params.userID,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: req.body.password
	}
	return UserController.update(args).then(updatedUser=> res.send(updatedUser))
})

router.delete('/:userID', (req,res)=>{
	return UserController.archive(req.params.userID).then(archived=>res.send(archived))
})

module.exports = router