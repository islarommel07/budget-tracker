const express = require('express');
const router = express.Router();
const TransactionController = require('../controllers/transactions')
const auth = require('../auth')

// create transaction
router.post('/', auth.verify, (req,res)=>{
	const args = {
		userId: auth.decode(req.headers.authorization).id,
		categoryName: req.body.categoryName,
		description: req.body.description,
		amount: req.body.amount
	}
	TransactionController.add(args).then(result=> res.send(result))
})

// retrieve transaction
router.get('/', (req,res)=>{
	TransactionController.getAll().then(results=>res.send(results))
})

router.get('/:transactionID', (req,res)=>{
	TransactionController.getOne(req.params.transactionID).then(result=> res.send(result))
})

// update transaction
router.put('/:transactionID', auth.verify, (req,res)=>{
	const args = {
		userId: auth.decode(req.headers.authorization).id,
		transactionID: req.params.transactionID,
		transaction:[{
			transactionName: req.body.transactionName,
			description: req.body.description,
			amount: req.body.amount	
		}]
	}
	TransactionController.update(args).then(result=>res.send(result))
})

// delete transaction

router.delete('/:transactionID', auth.verify, (req,res)=>{
	const args = {
		userId: auth.decode(req.headers.authorization).id,
		transactionID: req.params.transactionID
	}
	TransactionController.archive(args).then(result=> res.send(result))
})


module.exports = router