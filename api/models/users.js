const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
	password: {
		type: String,
	},
	loginType: {
		type: String
	},
	category:[{
		categoryId: {
			type: String
		},
		name: {
			type:String
		},
		categoryType: {
			type: String
		},
		isActive: {
			type: Boolean,
			default: true
		}
	}],
	transaction: [{
		isActive: {
			type: Boolean,
			default: true
		},
		transactionId:{
			type: String
			
		},
		categoryName: {
			type: String
		},
		description: {
			type: String
		},
		amount: {
			type: Number
		},
		createdAt: {
			type: Date
		},
		balanceAfterTransaction: {
			type: Number
		}
	}],
	currentBalance:{
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true
	}


});

module.exports = mongoose.model('Users', UserSchema);