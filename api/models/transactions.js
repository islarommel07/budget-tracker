const mongoose = require('mongoose');


const TransactionSchema = new mongoose.Schema({
	isActive: {
		type: Boolean,
		default: true
	},	
	transaction: [{
			categoryName: {
				type: String,
				required: [true, 'category name name is required']
			},
			description: {
				type: String,
				required: [true, 'indicate the nature of transaction']
			},
			amount: {
				type: Number,
				required: [true, 'amount is required']
			},
			createdAt: {
				type: Date,
				default: Date.now
			},
			balanceAfterTransaction: {
				type: Number
			}
		}
	],
	transactors: [{
		userId: {
			type: String
		},
		transactionDate: {
			type: Date,
			default: new Date()
		}
	}]
})

module.exports = mongoose.model('Transactions', TransactionSchema);