const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
	isActive: {
		type: Boolean,
		default: true
	},
	category: [{
			name: {
				type: String,
				required: [true, 'name is required']
			},
			type: {
				type: String,
				required: [true, 'indicate if income or expense']
			}
		}
	],
	transactions: [{
		transactionId: {
			type: String
		}
	}]
})

module.exports = mongoose.model('Category', CategorySchema);	