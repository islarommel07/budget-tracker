import {useState, useEffect, useContext} from 'react'
import {Container,Row, Col, Button, Form} from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import { GoogleLogin } from 'react-google-login'
import Swal from 'sweetalert2'

export default function login() {
	const {setUser} = useContext(UserContext)	

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password])

	function authenticate(e){
		e.preventDefault()
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken)
				fetch('http://localhost:4000/api/users/details',{
					headers: {
						'Authorization' : `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					setUser({
						id: data._id
					})
					Router.push('/dashboard')
				})
			} else {
				Router.push('/error')
			}
		})
	}

	const captureLoginResponse = (response) => {
		fetch('http://localhost:4000/api/users/verify-google-id-token', {
			method: 'POST',
			headers: {
				'Contente-Type': 'application/json'
			},
			body: JSON.stringify({
				tokenId: response.tokenId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken)
				setUser({id: data._id})
				Router.push('/dashboard')
			} else {
				if(data.error == 'google-auth-error') {
					Swal.fire(
						'Google Auth Error',
						'Google authentication procedure failed.',
						'error'
						)
				} else if (data.error === 'login-type-error') {
					Swal.fire(
						'Login Type Error',
						'You may have registered through a different login procedure.',
						'error'
						)
				}
			}
		})
	}

	return(
		<Container>
		<Row>
			<Col>
				<Form onSubmit = {e => authenticate(e)}>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control
							type = "email"
							onChange = {e => setEmail(e.target.value)}
							value = {email}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control
							type = "password"
							onChange = {e => setPassword(e.target.value)}
							value = {password}
						/>
					</Form.Group>
					<Button className="mb-2" variant = "success" type = "submit" disabled={isDisabled}>Login</Button>
				</Form>
				<GoogleLogin
					clientId='438451252412-n9cqnsgcrkqng28dfuaas3l72qh752fo.apps.googleusercontent.com' 
		            onSuccess={captureLoginResponse} 
		            onFailure={captureLoginResponse} 
		            cookiePolicy={'single_host_origin'}
				/>
			</Col>
		</Row>
		</Container>
		)
}