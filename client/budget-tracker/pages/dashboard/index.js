import { useContext, useEffect, useState } from 'react'
import { Jumbotron, Form, Button, Container, Card, Table, Col } from 'react-bootstrap'
import Greeting from '../../components/UserName'
import TransactionList from '../../components/TransactionList'
import UserContext from '../../UserContext'
import Router from 'next/router'

export default function dashboard({result}){
	const {user} = useContext(UserContext)
	const [firstName, setFirstName] = useState('')

	const [categoryName, setCategoryName] = useState('')
	const [description, setDescription] = useState('')
	const [amount, setAmount] = useState('')
	const [isDisabled, setIsDisabled] = useState (true)
	const [watcher, setWatcher] = useState(false)

	useEffect(() =>{
		if(categoryName !== '' && description !== '' && amount !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [categoryName, description, amount])

	useEffect(() => {
		fetch('http://localhost:4000/api/users/details', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				console.log(data)
				setFirstName(
					 data.firstName

				)
			}
		})
	},[])

	function createTransaction(e){
		e.preventDefault()
	fetch('http://localhost:4000/api/transactions', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json',
			'Authorization' : `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			categoryName: categoryName,
			description: description,
			amount: amount
		})
	})
	.then(res => res.json())
	.then(data => {
		setCategoryName('')
		setDescription('')
		setAmount('')
		setWatcher(true)
		Router.push('/dashboard')
		console.log(data)
		// Router.push('/r')
	})
}


	return (
		<>
		<Jumbotron>
			<h1>Hi {firstName}!</h1>
			<hr/>
			<h3>Your current balance is:</h3>
			<h4>Total Income:</h4>
			<h4>Total Expense:</h4>
		</Jumbotron>
		<Card>
			<Card.Body>
				<Form onSubmit ={e => createTransaction(e)}>
					<Form.Group>
						<Form.Control
							type="text"
							onChange={e => setCategoryName(e.target.value)}
							value={categoryName}
							placeholder="Category Name"
						/>
					</Form.Group>
					<Form.Group>
						<Form.Control
							type="text"
							onChange={e => setDescription(e.target.value)}
							value={description}
							placeholder="Description"
						/>
					</Form.Group>
					<Form.Group>
						<Form.Control
							type="number"
							onChange={e =>setAmount(e.target.value)}
							value={amount}
							placeholder="Amount"
						/>
					</Form.Group>
					<Button type="submit" disabled={isDisabled}>Transact</Button>
				</Form>
			</Card.Body>
		</Card>
			{
				result.map(list =>{
					return(
						<Col>
							<TransactionList transact={list}/>
						</Col>
						)
				})
			}
		
		</>

		)
}

export async function getServerSideProps(){
	const res = await fetch("http://localhost:4000/api/transactions", {
		"method" : "GET",
		"headers" : {
			"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYTA3MGI0NDFlYjVhMTljMGNlMTY0MyIsImVtYWlsIjoibmVAbWFpbC5jb20iLCJpYXQiOjE2MDQzNTAxNDJ9._twa-amJ_fnqh7P_BBOBXeviX4LkgK7s03cH4W0S4Fk"
		}
	})
	const result = await res.json()
	return {
		props: {
			result
		}
	}
}