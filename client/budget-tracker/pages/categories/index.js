import { useState, useEffect, useContext }  from 'react'
import { Form, Button, Row, Container, Card, Col} from 'react-bootstrap'
import UserContext from '../../UserContext'
import CategoryList from '../../components/CategoryList'
import Router from 'next/router'


export default function index({result}){
	const { token } = useContext(UserContext)
	const { user } = useContext(UserContext)
	const [name, setName] = useState('')
	const [type, setType ] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)
	const [categories, setCategories] = useState([])
	const [watcher, setWatcher] = useState(false)
	console.log(result)


	useEffect(() => {
		if(name !=='' & type !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [name, type])


	function createCategory(e){
		e.preventDefault()
		fetch('http://localhost:4000/api/category',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`				
			},
			body: JSON.stringify({
				name: name,
				type: type
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
				setName('')
				setType('')
				setWatcher(true)
				Router.push('/categories')
			}
		})
	}

	return(
		<>
		<Card>
			<Card.Body>
				<Container className = "mt-4">
					<Row>
						 <Form onSubmit = {e =>createCategory(e)}>
							<Form.Row>
								<Form.Group className="inline">
									<Form.Control
										type = 'text'
										placeholder = "Category Name"
										onChange = {e => setName(e.target.value)}
										value = {name}
									/>
								</Form.Group>
								<Form.Group className="inline ml-4">
									<Form.Control
										type = 'text'
										placeholder = "Income or Expense?"
										onChange = {e => setType(e.target.value)}
										value = {type}
									/>						
								</Form.Group>
								<Button type= "submit" className ="ml-4" disabled ={isDisabled}>Add</Button>
							</Form.Row>				
						</Form>
					</Row>
				</Container>
			</Card.Body>
		</Card>
	
			{
				result.map(cat => {
					return(
						<Col>
							<CategoryList load={cat}/>
						</Col>
						)
				})
			}
		</>
		)
}

export async function getServerSideProps(){
	const res = await fetch("http://localhost:4000/api/category", {
		"method" : "GET",
		"headers" : {
			"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYTA3MGI0NDFlYjVhMTljMGNlMTY0MyIsImVtYWlsIjoibmVAbWFpbC5jb20iLCJpYXQiOjE2MDQzNTAxNDJ9._twa-amJ_fnqh7P_BBOBXeviX4LkgK7s03cH4W0S4Fk"
		}
	})
	const result = await res.json()
	return {
		props: {
			result
		}
	}
}