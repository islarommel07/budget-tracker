import {useContext, useEffect} from 'react'
import UserContext from '../UserContext'
import Router from 'next/router'

export default function logout(){
	const unSetUser = useContext(UserContext)

	useEffect(()=> {
		unSetUser()
		Router.push('/login')
	}, [])
	return null
}