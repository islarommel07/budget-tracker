import {useState, useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import {Container} from 'react-bootstrap'
import Head from 'next/head'
import AppNavBar from '../components/AppNavBar'
import { UserProvider } from '../UserContext'

function MyApp({ Component, pageProps }) {
	const [token, setToken] = useState({
		token: null
	})
	const [user, setUser] = useState({
		id: null
	})

	const unSetUser = () => {
		localStorage.clear()
		setUser({
			id:null
		})
	}

	useEffect(() => {
		fetch('http://localhost:4000/api/users/details', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data._id) {
				setUser({
					id: data._id
				})
				setToken({
					token:`Bearer ${localStorage.getItem('token')}`
				})
			} else {
				setUser({
					id: null
				})
			}
		})
	}, [user.id])

  return (
  	<div>
	  	<Head>
	  		<title>Budget Tracker</title>
	  		<meta name="viewport" content="initial-scale=1.0, width=device-width" />
	  	</Head>
	  	<UserProvider value={{user,setUser,unSetUser,token}}>
	  	<AppNavBar/>
	  	<Container>
	  		<Component {...pageProps} />
	  	</Container>
	  	</UserProvider>
  	</div>
  	)
}

export default MyApp

