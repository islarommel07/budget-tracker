import { useState, useEffect } from 'react'
import {Row, Col, Form, Button } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'

export default function register (){
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(()=> {
		if (firstName !== '' && lastName !== '' && email !=='' && password !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password])

	function registerUser(e){
		e.preventDefault()
		fetch('http://localhost:4000/api/users', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.error) {
				if(data.error==='use-google-login'){
					Swal.fire(
						'Google Account Registered',
						'You may have registered through a different procedure.',
						'error'
						)
				} else if (data.error === 'account-already-registered'){
					Swal.fire(
						'Already Registered',
						'Your email has already registered on your app',
						'error'
						)
				}
			} else {
				Router.push('/login')
			}
		})
	}

	return(
		<Row>
			<Col md={8} className="mx-auto">
				<Form onSubmit={(e) => registerUser(e)} className = "mt-4">
					<Form.Group>
						<Form.Label>First Name:</Form.Label>
						<Form.Control
							type="text"
							onChange = {e => setFirstName(e.target.value)}
							value ={firstName}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name:</Form.Label>
						<Form.Control
							type="text"
							onChange = {e => setLastName(e.target.value)}
							value = {lastName}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control
							type = "text"
							onChange = {e => setEmail(e.target.value)}
							value = {email}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control
							type = "password"
							onChange = {e => setPassword(e.target.value)}
							value = {password}
						/>
					</Form.Group>
					<Button type= "submit" disabled={isDisabled}>Register</Button>
				</Form>
			</Col>
		</Row>
		)
}