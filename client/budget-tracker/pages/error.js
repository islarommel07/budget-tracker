import {Jumbotron} from 'react-bootstrap'
import Link from  'next/link'
export default function error(){
	return(
		<Jumbotron>
			<h1>Oops!</h1>
			<p>Something went wrong! Please try again later!</p>
			<Link href="/register"><a className="btn btn-primary">Go back to registration page</a></Link>
		</Jumbotron>
		)
}