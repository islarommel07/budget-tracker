import {Jumbotron} from 'react-bootstrap'

export default function Greeting({greet}){
	console.log(greet)
	return (
		<Jumbotron>
			<h1>Hi {greet.firstName}</h1>
		</Jumbotron>
		)
}