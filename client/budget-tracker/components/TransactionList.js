import {Container, Table} from 'react-bootstrap'

export default function TransactionList({transact}){
	console.log(transact.transaction[0].categoryName)
	return (
		<Container>
			<Table striped bordered hover variant="dark">
				<thead>
					<tr>
						<th>Date</th>
						<th>Category</th>
						<th>Description</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{transact.transaction[0].createdAt}</td>
						<td>{transact.transaction[0].categoryName}</td>
						<td>{transact.transaction[0].description}</td>
						<td>{transact.transaction[0].amount}</td>
					</tr>
				</tbody>
			</Table>
		</Container>
		)
}