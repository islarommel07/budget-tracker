import{useContext} from 'react'
import {Card, Table, Button} from 'react-bootstrap'
import UserContext from '../UserContext'

export default function CategoryList({load}){
	const { user } = useContext(UserContext)

	return(
		<Card>
			<Card.Body>
				<Card.Text>{load.category[0].name}</Card.Text>
				<Card.Text>{load.category[0].type}</Card.Text>
			</Card.Body>
		</Card>
		)

}