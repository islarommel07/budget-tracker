import {useContext} from 'react'
import {Nav, Navbar} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function AppNavBar(){
	const{user} = useContext(UserContext)

	return (
			<Navbar bg="light" expand="lg">
				<Navbar.Brand href="/">Budget Tracker</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
						{
							(user.id !== null) ?
							<>
								<Nav.Link href="/dashboard">Dashboard</Nav.Link>
								<Nav.Link href="/categories">Categories</Nav.Link>
								<Nav.Link href="/trend">Trend</Nav.Link>
								<Nav.Link href="/breakdown">Breakdown</Nav.Link>
								<Nav.Link href="/logout">Logout</Nav.Link>
							</>
							:
							<>
								<Nav.Link href="/login">Login</Nav.Link>
								<Nav.Link href="/register">Register</Nav.Link>
							</>

						}						
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}